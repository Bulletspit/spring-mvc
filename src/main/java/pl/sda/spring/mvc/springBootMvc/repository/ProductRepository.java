package pl.sda.spring.mvc.springBootMvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.spring.mvc.springBootMvc.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
