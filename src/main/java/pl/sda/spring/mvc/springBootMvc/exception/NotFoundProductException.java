package pl.sda.spring.mvc.springBootMvc.exception;

public class NotFoundProductException extends ProductException{
    public NotFoundProductException() {
    }

    public NotFoundProductException(String message) {
        super(message);
    }
}
