package pl.sda.spring.mvc.springBootMvc.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Builder
public class UserOrderDTO {
  private long id;
  private LocalDate date;
  private List<ProductDTO> products = new ArrayList<>();
}
