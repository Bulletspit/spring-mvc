package pl.sda.spring.mvc.springBootMvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.spring.mvc.springBootMvc.exception.UserNotFoundException;
import pl.sda.spring.mvc.springBootMvc.service.UserOrderService;

@Controller
@RequestMapping("/orders")
public class OrderConroller {
  private final UserOrderService userOrderService;

  @Autowired
  public OrderConroller(UserOrderService userOrderService) {
    this.userOrderService = userOrderService;
  }

  @RequestMapping
  public ModelAndView getOrdersView() throws UserNotFoundException {
    ModelAndView mnv = new ModelAndView("orders");
    mnv.addObject("orders", userOrderService.getOrderByUserLogin("user"));
    return mnv;
  }

}
