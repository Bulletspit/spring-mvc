package pl.sda.spring.mvc.springBootMvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.spring.mvc.springBootMvc.dto.ModelMapper;
import pl.sda.spring.mvc.springBootMvc.dto.UserRegistrationDTO;
import pl.sda.spring.mvc.springBootMvc.model.User;
import pl.sda.spring.mvc.springBootMvc.repository.UserRepository;

@Service
public class UserService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  public User getUserByLogin(String login) throws Exception {
    return userRepository
        .findByLogin(login)
        .orElseThrow(() -> new Exception("Brak użytkownika o loginie" + login));
  }

  public boolean existUserWithLogin(String login) {
    return userRepository.findByLogin(login).isPresent();
  }

  public void createUser(UserRegistrationDTO userRegistrationDTO) {
    User user = ModelMapper.map(userRegistrationDTO);
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.save(user);
  }
}
