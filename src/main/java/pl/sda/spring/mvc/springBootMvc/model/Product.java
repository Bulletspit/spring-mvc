package pl.sda.spring.mvc.springBootMvc.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Product {
  @Id @GeneratedValue private long id;
  private String name;
  private String description;
  private BigDecimal price;

  public Product(String name, String description, BigDecimal price) {
    this.name = name;
    this.description = description;
    this.price = price;
  }
}
