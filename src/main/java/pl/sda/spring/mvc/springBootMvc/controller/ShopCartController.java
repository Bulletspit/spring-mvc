package pl.sda.spring.mvc.springBootMvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.spring.mvc.springBootMvc.exception.NotFoundProductException;
import pl.sda.spring.mvc.springBootMvc.component.ShopCart;
import pl.sda.spring.mvc.springBootMvc.exception.UserNotFoundException;
import pl.sda.spring.mvc.springBootMvc.service.ProductService;
import pl.sda.spring.mvc.springBootMvc.service.UserOrderService;

@Controller
@RequestMapping("/shopcart")
public class ShopCartController {
  private final ShopCart shopCart;
  private final UserOrderService userOrderService;

  @Autowired
  public ShopCartController(ShopCart shopCart, UserOrderService userOrderService) {
    this.shopCart = shopCart;
    this.userOrderService = userOrderService;
  }

  @ModelAttribute("shopCart")
  public ShopCart getShopCart() {
    return shopCart;
  }

  @GetMapping("/addproduct")
  public String addProductToShopCart(@RequestParam(name = "idproduct") String idProduct)
      throws NotFoundProductException {
    System.out.println("dodawanie produktu do koszyka");
    Long idProductLong = Long.valueOf(idProduct);
    shopCart.addProduct(idProductLong);
    return "redirect:/products";
  }

  @GetMapping
  public ModelAndView displayShopCart() {
    ModelAndView mnv = new ModelAndView("shopCart");
    return mnv;
  }

  @GetMapping("/placeorder")
  public String placeOrder() throws NotFoundProductException, UserNotFoundException {
    userOrderService.createNewOrder("user", shopCart.getProducts());
    return "redirect:/orders";
  }
}
