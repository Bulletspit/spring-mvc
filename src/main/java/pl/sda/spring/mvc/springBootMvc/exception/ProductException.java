package pl.sda.spring.mvc.springBootMvc.exception;

public class ProductException extends Exception {
    public ProductException(){
    }

    public ProductException(String message) {
        super(message);
    }
}
