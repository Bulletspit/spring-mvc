package pl.sda.spring.mvc.springBootMvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {
    @RequestMapping
    public ModelAndView getIndex() {
        ModelAndView mvn = new ModelAndView();
        mvn.addObject("userName", "Piotrek");
        mvn.setViewName("index");
        return mvn;
    }
}
