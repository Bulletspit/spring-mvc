package pl.sda.spring.mvc.springBootMvc.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.spring.mvc.springBootMvc.exception.ProductException;

@ControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(value = ProductException.class)
    public ModelAndView productException(Exception ex) {
        ModelAndView mnv = new ModelAndView("error");
        mnv.addObject("errorMessage", ex.getMessage());
        return mnv;
    }

    @ExceptionHandler(value = Exception.class)
    public ModelAndView basicException(Exception ex) {
        ex.printStackTrace();
        ModelAndView error = new ModelAndView("error");
        error.addObject("errorMessage", ex.getMessage());
        return error;
    }



}
