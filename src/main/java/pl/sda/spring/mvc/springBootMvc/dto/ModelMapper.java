package pl.sda.spring.mvc.springBootMvc.dto;

import pl.sda.spring.mvc.springBootMvc.model.Product;
import pl.sda.spring.mvc.springBootMvc.model.User;
import pl.sda.spring.mvc.springBootMvc.model.UserOrder;

import java.util.stream.Collectors;

public class ModelMapper {

  private ModelMapper() {}

  public static ProductDTO map(Product prduct) {
    return ProductDTO.builder()
        .id(prduct.getId())
        .name(prduct.getName())
        .description(prduct.getDescription())
        .price(prduct.getPrice())
        .build();
  }

  public static Product map(ProductDTO productDTO) {
    return Product.builder()
        .id(productDTO.getId())
        .name(productDTO.getName())
        .description(productDTO.getDescription())
        .price(productDTO.getPrice())
        .build();
  }

  public static UserOrderDTO map(UserOrder userOrder) {
    return UserOrderDTO.builder()
        .id(userOrder.getId())
        .date(userOrder.getDate())
        .products(
            userOrder.getProducts().stream().map(ModelMapper::map).collect(Collectors.toList()))
        .build();
  }

  public static User map(UserRegistrationDTO userRegistrationDTO) {
    return User.builder()
        .login(userRegistrationDTO.getLogin())
        .password(userRegistrationDTO.getPassword())
        .build();
  }
}
