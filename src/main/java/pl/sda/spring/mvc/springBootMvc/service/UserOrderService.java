package pl.sda.spring.mvc.springBootMvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.sda.spring.mvc.springBootMvc.dto.ModelMapper;
import pl.sda.spring.mvc.springBootMvc.dto.ProductDTO;
import pl.sda.spring.mvc.springBootMvc.dto.UserOrderDTO;
import pl.sda.spring.mvc.springBootMvc.exception.NotFoundProductException;
import pl.sda.spring.mvc.springBootMvc.exception.UserNotFoundException;
import pl.sda.spring.mvc.springBootMvc.model.Product;
import pl.sda.spring.mvc.springBootMvc.model.UserOrder;
import pl.sda.spring.mvc.springBootMvc.repository.ProductRepository;
import pl.sda.spring.mvc.springBootMvc.repository.UserOrderRepository;
import pl.sda.spring.mvc.springBootMvc.repository.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserOrderService {
  private final UserRepository userRepository;
  private final ProductRepository productRepository;
  private final UserOrderRepository userOrderRepository;

  @Autowired
  public UserOrderService(
      UserRepository userRepository,
      ProductRepository productRepository,
      UserOrderRepository userOrderRepository) {
    this.userRepository = userRepository;
    this.productRepository = productRepository;
    this.userOrderRepository = userOrderRepository;
  }

  public void createNewOrder(String userLogin, List<ProductDTO> products)
      throws NotFoundProductException, UserNotFoundException {

    UserOrder userOrder = new UserOrder();
    userOrder.setUser(
        userRepository
            .findByLogin(userLogin)
            .orElseThrow(
                () ->
                    new UsernameNotFoundException("Nie znaleziono uzera o loginie " + userLogin)));
    userOrder.setDate(LocalDate.now());
    List<Product> productEntity = new ArrayList<>();
    for (ProductDTO product : products) {
      productEntity.add(
          productRepository
              .findById(product.getId())
              .orElseThrow(
                  () ->
                      new NotFoundProductException(
                          "Nie znaleziono produktu o id " + product.getId())));
    }
    userOrder.setProducts(productEntity);
    userOrderRepository.save(userOrder);
  }

  public List<UserOrderDTO> getOrderByUserLogin(String userLogin) throws UserNotFoundException {
    return userOrderRepository
        .findByUser_Login(userLogin)
        .stream()
        .map(ModelMapper::map)
        .collect(Collectors.toList());
    //    return userRepository
    //        .findByLogin(userLogin)
    //        .orElseThrow(
    //            () -> new UserNotFoundException("Nie znaleziono uzytownika o loginie " +
    // userLogin))
    //        .getUserOrders()
    //        .stream()
    //        .map(ModelMapper::map)
    //        .collect(Collectors.toList());
  }
}
