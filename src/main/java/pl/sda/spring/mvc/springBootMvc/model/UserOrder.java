package pl.sda.spring.mvc.springBootMvc.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Entity
@Data
public class UserOrder {
    @Id
    @GeneratedValue
    private long id;
    private LocalDate date;
    @ManyToOne
    @JoinTable(name = "id_user")
    private User user;
    @ManyToMany
    @JoinTable(name = "PRODUCT_ORDER",
            joinColumns = {@JoinColumn(name = "id_user_order")},
            inverseJoinColumns = {@JoinColumn(name = "id_product")})
    List<Product> products = new ArrayList<>();
}
