package pl.sda.spring.mvc.springBootMvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.spring.mvc.springBootMvc.dto.ModelMapper;
import pl.sda.spring.mvc.springBootMvc.dto.ProductDTO;
import pl.sda.spring.mvc.springBootMvc.exception.NotFoundProductException;
import pl.sda.spring.mvc.springBootMvc.model.Product;
import pl.sda.spring.mvc.springBootMvc.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {
  private final ProductRepository productRepository;

  @Autowired
  public ProductService(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  public List<ProductDTO> getProducts() {
    return productRepository.findAll().stream().map(ModelMapper::map).collect(Collectors.toList());
  }

  public ProductDTO getProductById(long id) throws NotFoundProductException {
    return productRepository
        .findById(id)
        .map(ModelMapper::map)
        .orElseThrow(() -> new NotFoundProductException("Nie znaleziono produktu o id " + id));
  }

  public void addNewProduct(ProductDTO productDTO) {
    productRepository.save(ModelMapper.map(productDTO));
  }

  public List<Product> findAllById(List<Long> productsId) {
    return productRepository.findAllById(productsId);
  }
}
