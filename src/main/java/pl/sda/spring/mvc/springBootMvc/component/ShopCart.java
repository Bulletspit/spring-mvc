package pl.sda.spring.mvc.springBootMvc.component;

import lombok.Data;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import pl.sda.spring.mvc.springBootMvc.dto.ProductDTO;
import pl.sda.spring.mvc.springBootMvc.exception.NotFoundProductException;
import pl.sda.spring.mvc.springBootMvc.model.Product;
import pl.sda.spring.mvc.springBootMvc.service.ProductService;

import java.util.ArrayList;
import java.util.List;

@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
@Data
@ToString
public class ShopCart {

    List<ProductDTO> products = new ArrayList<>();

    private final ProductService productService;

    @Autowired
    public ShopCart(ProductService productService) {
        this.productService = productService;
    }

    public void addProduct(Long idProduct) throws NotFoundProductException {
        ProductDTO productDTO = productService.getProductById(idProduct);
        products.add(productDTO);

    }
}
