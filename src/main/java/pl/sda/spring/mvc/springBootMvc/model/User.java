package pl.sda.spring.mvc.springBootMvc.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue
    private long id;

    private String login;
    private String password;



    @OneToMany(mappedBy = "user")
    private List<UserOrder> userOrders;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
