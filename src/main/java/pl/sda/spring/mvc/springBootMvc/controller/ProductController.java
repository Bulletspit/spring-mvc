package pl.sda.spring.mvc.springBootMvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.spring.mvc.springBootMvc.exception.NotFoundProductException;
import pl.sda.spring.mvc.springBootMvc.component.ShopCart;
import pl.sda.spring.mvc.springBootMvc.service.ProductService;

@Controller
@RequestMapping("/products")
public class ProductController {

    private final ShopCart shopCart;
    private final ProductService productService;

    @ModelAttribute("shopCart")
    private ShopCart getShopCart() {
        return shopCart;
    }

    @Autowired
    public ProductController(ProductService productService, ShopCart shopCart) {
        this.shopCart = shopCart;
        this.productService = productService;
    }

    @RequestMapping
    public ModelAndView getProductsView() {

        ModelAndView mnv = new ModelAndView();
        mnv.setViewName("products");
        mnv.addObject("products", productService.getProducts());

        return mnv;
    }

    @RequestMapping("/{id}")
    public ModelAndView getProductDetails(@PathVariable(name = "id") long id) throws NotFoundProductException {
        ModelAndView mnv = new ModelAndView("productDetails");
        mnv.addObject("product", productService.getProductById(id));

        return mnv;
    }
}

